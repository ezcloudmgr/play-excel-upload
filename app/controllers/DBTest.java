package controllers;

import java.util.Iterator;
import java.util.List;

import models.ProductImage;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

public class DBTest extends Controller {

	public static Result index() {

		List<ProductImage> prodImg = ProductImage.find.all();
		ProductImage productImage = null;

		for (Iterator<ProductImage> iterator = prodImg.iterator(); iterator
				.hasNext();) {
			productImage = (ProductImage) iterator.next();
			System.out.println(Json.toJson(productImage));
		}
		Form<ProductImage> form = Form.form(ProductImage.class);
		form = form.fill(productImage);
		return ok(views.html.index.render(form));
	}

	public static Result submit() {
		return ok("POST");
	}
}

package controllers;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jxl.Sheet;
import jxl.Workbook;
import models.SampleData;
import play.libs.F.Function0;
import play.libs.F.Promise;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import play.mvc.Result;

public class Application extends Controller {

	/*
	 * 回傳上傳選檔頁
	 */
	public static Promise<Result> upload() {

		return Promise.promise(new Function0<Result>() {
			@Override
			public Result apply() throws Throwable {
				return ok(views.html.upload.render());
			}
		});

	}

	/*
	 * 檢查上傳檔, 並讀出excel內容
	 */
	public static Promise<Result> checkFile() {

		return Promise.promise(new Function0<Result>() {

			@Override
			public Result apply() throws Throwable {

				MultipartFormData body = request().body().asMultipartFormData();
				FilePart excel1 = body.getFile("excel1");

				if (excel1 != null) {
					String fileName = excel1.getFilename();
					System.out.println(fileName);

					String contentType = excel1.getContentType();
					System.out.println(contentType);

					File file = excel1.getFile();

					/*
					 * 使用jxl讀取excel
					 * http://www.andykhan.com/jexcelapi/tutorial.html jar file
					 * 放在 lib/jxl.jar
					 */
					Workbook workbook;
					List<SampleData> datalist = new ArrayList<SampleData>();

					try {
						workbook = Workbook.getWorkbook(file);
						Sheet sheet = workbook.getSheet("sample_data");
						int rows = sheet.getRows();

						for (int row = 1; row < rows; row++) {
							SampleData cell = new SampleData();
							cell.no = sheet.getCell(0, row).getContents();
							cell.last_name = sheet.getCell(1, row)
									.getContents();
							cell.first_name = sheet.getCell(2, row)
									.getContents();
							cell.phone = sheet.getCell(3, row).getContents();
														
							/*
							 * Json Lib 可以使用play framework內附的 play.lib.Json
							 * 直接吃POJO to String.
							 */
							System.out.println(Json.toJson(cell).toString());
							
							datalist.add(cell);
						}

					} catch (Exception e) {
						e.printStackTrace();
					}

					return ok(views.html.list.render(datalist, fileName));
				} else {
					System.out.println("error Missing file");
					return redirect(routes.Application.upload());
				}
			}

		});
	}
}

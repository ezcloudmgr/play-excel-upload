package models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import play.data.validation.Constraints;
import play.db.ebean.Model.Finder;

@Entity
@Table(name = "tblpfpro_imgs")
public class ProductImage {

	@Constraints.Required
	public String pf_prod_no;

	@Constraints.Required
	public String vend_no;

	@Constraints.Required
	public Integer img_seqno;

	public String img_url;

	public String img_name;

	public String creator;

	public Date create_dt;

	public String moder;

	public Date mod_dt;

	public static Finder<String, ProductImage> find = new Finder<String, ProductImage>(
			String.class, ProductImage.class);

}

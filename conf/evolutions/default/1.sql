# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table tblpfpro_imgs (
  pf_prod_no                varchar(255),
  vend_no                   varchar(255),
  img_seqno                 integer,
  img_url                   varchar(255),
  img_name                  varchar(255),
  creator                   varchar(255),
  create_dt                 timestamp,
  moder                     varchar(255),
  mod_dt                    timestamp)
;




# --- !Downs

drop table tblpfpro_imgs;

